#!/bin/bash

debug=0
createdir()
{
	dir=$1
	if [ ! -d "$dir" ]
	then
		mkdir -p $dir
	fi
}


testcode(){
	echo "int main(){ int x,y; return 0;}" > test.c
	arg=$1
	gcc $arg -o test.out test.c 2> /dev/null
	#rm test.c
}

cleantmp(){
	if [ -d "test.c" ]
	then
		rm test.c
	fi
	if [ -d "test.out" ]
	then
		rm test.out
	fi
}

buildltp(){
	current_personality=$1
	builddir=$current_personality
	ltpdir=$2
	cf=$3
	eval $ltpdir/$build_dir/testcases/bin/read01 2>/dev/null
	if [ "$?" -eq "0" ]
	then
		return
	fi

	cd ltp
	if [ "$debug" -eq "0" ]
	then
		make autotools > ${current_personality}.log 2>/dev/null
		LDFLAGS=$cf CFLAGS=$cf ./configure --prefix=$ltpdir/$current_personality --host=$current_personality >> ${current_personality}.log 2>/dev/null
		if [ "$?" -ne "0" ]
		then
			echo "error on: LDFLAGS=$cf CFLAGS=$cf ./configure --prefix=$ltpdir/$current_personality "
			exit 1
		fi
		make -j 4 >> ${current_personality}.log 2>>${current_personality}.log #FIXME: errors not logged
		if [ "$?" -ne "0" ]
		then
			echo "error compiling, check ${current_personality}.log"
			exit 1
		fi
		make install >> ${current_personality}.log 2>/dev/null
		make clean >> ${current_personality}.log 2>/dev/null
	else
		make autotools
		cdir=`pwd`
		#CFLAGS=$cf ./configure --prefix=$ltpdir/$current_personality --host=$current_personality
		LDFLAGS=$cf CFLAGS=$cf ./configure --prefix=$ltpdir/$current_personality
		make -j 4
		make install
		make clean
	fi
	
	cd ..
}

getarchparams(){
	archt=$1
	cc=$2
	case $archt in
		"i386")
			flags=("-m32")
			personalities=("i386")
			;;
		"x32")
			flags=("-mx32" "-m32")
			personalities=("x32" "i386")
			;;
		"x86_64")
			flags=("-m64" "-mx32" "-m32")
			personalities=("x86_64" "x32" "i386")
			;;
		"sparc64")
			flags=("-m64" "-m32")
			personalities=("sparc64" "sparc")
			;;
		"sparc")
			flags=("-m32")
			personalities=("sparc")
			;;
		#"aarch64")
			#	flags=("aarch64" "arm")
			#	personalities=("" "")
			#	;;
		"powerpc64")
			flags=("powerpc64" "powerpc")
			personalities=("-m64" "-m32")
			;;
		"powerpc")
			flags=("powerpc")
			personalities=("-m32")
			;;
		"tilegx")
			flags=("tilegx" "tilepro")
			personalities=("-m64" "-m32")
			;;
		"tilepro")
			flags=("tilepro")
			personalities=("-m32")
			;;
		"arm")
			flags=("$archt")
			personalities=("")
			;;
		"avr32")
			flags=("$archt")
			personalities=("")
			;;
		"bfin")
			flags=("$archt")
			personalities=("")
			;;
		"hppa")
			flags=("$archt")
			personalities=("")
			;;
		"ia64")
			flags=("$archt")
			personalities=("")
			;;
		"m68k")
			flags=("$archt")
			personalities=("")
			;;
		"metag")
			flags=("$archt")
			personalities=("")
			;;
		"microblaze")
			flags=("$archt")
			personalities=("")
			;;
		"metag")
			flags=("$archt")
			personalities=("")
			;;
		"microblaze")
			flags=("$archt")
			personalities=("")
			;;
		"mips")
			flags=("$archt")
			personalities=("")
			;;
		"or1k")
			flags=("$archt")
			personalities=("")
			;;
		"s390")
			flags=("$archt")
			personalities=("")
			;;
		"s390x")
			flags=("$archt")
			personalities=("")
			;;
		"sh")
			flags=("$archt")
			personalities=("")
			;;
		"sh64")
			flags=("$archt")
			personalities=("")
			;;
		"xtensa")
			flags=("$archt")
			personalities=("")
			;;
		\?)
			echo "Invalid architecture"
			exit 1
			;;
	esac

	if [ "$cc" -eq "1" ]	#required
	then
		a=1

	elif [ "$cc" -eq "2" ]	#local
	then
		h_flags=(${flags[@]})
		h_personalities=(${personalities[@]})
	fi

}

arch=`uname -m`
testfile="strace_tests"
ltpdir="/tmp/ltp"
while getopts ":a:d:f:hl" o
do
	case $o in
		a)
			arch=$OPTARG
			;;
		d)
			debug=1
			;;
		f)
			testfile=$OPTARG
			;;
		h)
			#cat << EOF
			#asdfasdflsadlfasdflkasdfasdfasdf
			#EOF
			cat doc
			exit 0
			;;
		l)
			cat << EOF
Supported architectures for testing:
	x86_64
	x32
	i386
EOF
			exit 0
			;;
		\?)
			echo "Invalid option $OPTARG"
			;;
	esac
done

#declare -A archstable=( ["x86_64"]=("x32" "i386") ["aarch64"]=("arm") ["arm"]=() )

getarchparams `uname -m` 2 #sets a_flags and a_personalities
getarchparams $arch 1 #sets flags and personalities

git clone -b 20140422 https://github.com/linux-test-project/ltp.git

qemu=1
for f in ${h_personalities[@]}
do
	if [ "${personalities[0]}" = "$f" ]
	then
		qemu=0
		break
	fi
done
if [ "$qemu" -eq "0" ]
then
	i=0
	for f in ${flags[@]}
	do
		echo "Current flag: $f"
		echo "################"
		current_personality=${personalities[$i]}
		testcode $f 
		if [ "$?" -eq "0" ]
		then
			buildltp ${current_personality} $ltpdir $f
			IFS=$'\x0a'
			for ts in `awk '!/^#/' $testfile `
			do
				echo $ts
				dir="log/$arch"
				date=`date +%s`
				createdir $dir
				eval $ltpdir/$current_personality/testcases/bin/$ts > $dir/${current_personality}_$date.log
				sleep 1
			done

		else
			echo "$f not supported by gcc"
		fi
		cleantmp
		let "i += 1"
	done
else
	# test over qemu 
	git clone https://eds000n@git.code.sf.net/p/straceimages
	$arch/start.sh	
fi
