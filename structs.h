
struct stack_t32{
	uint32_t ss_sp;
	int32_t ss_flags;
	uint32_t ss_size;
};
typedef strace_stack_t stack_t;
typedef strace_stack_t stack_t32;

print_stack_t(struct tcb *tcp, unsigned long addr)
{
	stack_t ss;
	int r;

	if (!addr) {
		tprints("NULL");
		return;
	}

#if SUPPORTED_PERSONALITIES > 1 && SIZEOF_LONG > 4
	if (current_wordsize != sizeof(ss.ss_sp) && current_wordsize == 4) {
		struct {
			uint32_t ss_sp;
			int32_t ss_flags;
			uint32_t ss_size;
		} ss32;
		r = umove(tcp, addr, &ss32);
		if (r >= 0) {
			memset(&ss, 0, sizeof(ss));
			ss.ss_sp = (void*)(unsigned long) ss32.ss_sp;
			ss.ss_flags = ss32.ss_flags;
			ss.ss_size = (unsigned long) ss32.ss_size;
		}
	} else
#endif
	{

#include "defs.h"
#include <sys/user.h>
#include <fcntl.h>

#ifdef HAVE_SYS_REG_H
# include <sys/reg.h>
#elif defined(HAVE_LINUX_PTRACE_H)
# undef PTRACE_SYSCALL
# ifdef HAVE_STRUCT_IA64_FPREG
#  define ia64_fpreg XXX_ia64_fpreg
# endif
# ifdef HAVE_STRUCT_PT_ALL_USER_REGS
#  define pt_all_user_regs XXX_pt_all_user_regs
# endif
# ifdef HAVE_STRUCT_PTRACE_PEEKSIGINFO_ARGS
#  define ptrace_peeksiginfo_args XXX_ptrace_peeksiginfo_args
# endif
# include <linux/ptrace.h>
# undef ptrace_peeksiginfo_args
# undef ia64_fpreg
# undef pt_all_user_regs
#endif

#ifdef IA64
# include <asm/ptrace_offsets.h>
#endif

#if defined(SPARC) || defined(SPARC64) || defined(MIPS)
typedef struct {
	struct pt_regs		si_regs;
	int			si_mask;
} m_siginfo_t;
#elif defined HAVE_ASM_SIGCONTEXT_H
# if !defined(IA64) && !defined(X86_64) && !defined(X32)
#  include <asm/sigcontext.h>
